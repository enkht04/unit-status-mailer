#!/bin/bash

cp mailer_unit.sh /usr/local/bin/ && chmod 755 /usr/local/bin/mailer_unit.sh &&
cp unit-status-mailer@.service /etc/systemd/system/ && chmod 644 /etc/systemd/system/unit-status-mailer@.service &&
cp .envs_mailer /etc/exim4/ && cmod 644 /etc/exim4/.envs_mailer
