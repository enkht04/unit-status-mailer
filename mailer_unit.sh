#!/bin/bash

## hardcoding path of binaries
GPG="$(command -v gpg)"
SYSTEMCTL=$(command -v systemctl)
##Get current values of following commands
DATE=$(/bin/date)
HOSTNAME=$(/bin/hostname)

## System Parameters
## $1 is systemd's %i (service-name invoking this script)
UNIT="${1}"
UNITSTATUS=$( "${SYSTEMCTL}" status "$UNIT")
#LOCKFILE=/var/lock/unit_mail
## ensure var is set and has a default value of 0
RETVAL=0
ENCRYPTED="false"

## Mail Args
source "/etc/exim4/.envs_mailer"
SUBJECT="Status mail for unit: $UNIT"

BODY="This is an automated message to notify you that on  ${HOSTNAME} ${UNIT} has failed!

${UNITSTATUS}
Date and Time: ${DATE} "

ENC="${GPG} --batch --armor --recipient ${EMAIL} --encrypt"

send()
{	echo -n "Sending Unit notify mail: "
	
## Check if message has to be sent encrypted or in plaintext
	
	if [ "${ENCRYPTED}" = true ]; then
		echo "${BODY}" | "${ENC}" | mail -s "${SUBJECT}" "${EMAIL}"
		RETVAL=$?
	else 
		echo "${BODY}" | mail -s "${SUBJECT}" "${EMAIL}"
		RETVAL=$?
	fi
	
 	if [ "${RETVAL}" -eq 0 ]; then
#		rm -f "${LOCKFILE}"
		echo "Mail sent correctly"
	else
		echo "Mail failed miserably"
	fi

	echo
	return "${RETVAL}"
}

send;
/usr/sbin/exim -qff
exit "${RETVAL}"
