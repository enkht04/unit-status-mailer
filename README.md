# Unit Status Mailer

A simple unit to invoke from other systemd service in order to get a notification on a service event

### NB 
Added exim queue flush due to forking issue:
https://systemd-devel.freedesktop.narkive.com/nV1QMO8j/exim4-only-queues-mails-sent-by-systemd-service
https://serverfault.com/questions/982318/mail-stuck-in-exim-queue-for-no-reason
